# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.auto_update = false
  end

  # IP Fabric
  config.vm.define("spine1") do |spine1|
    spine1.vm.box = "CumulusCommunity/cumulus-vx"
    spine1.vm.box_version = "3.7.3"
    spine1.vm.network("forwarded_port", guest: 22, host: 2022, id: 'ssh', auto_correct: false)

    # Fabric links (swp1 - swp4)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF1", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF2", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF3", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    spine1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF4", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)
  end

  config.vm.define("spine2") do |spine2|
    spine2.vm.box = "CumulusCommunity/cumulus-vx"
    spine2.vm.box_version = "3.7.3"
    spine2.vm.network("forwarded_port", guest: 22, host: 2023, id: 'ssh', auto_correct: false)

    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF1", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF2", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)
    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF3", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    spine2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF4", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)

  end

  config.vm.define("leaf1") do |leaf1|
    leaf1.vm.box = "CumulusCommunity/cumulus-vx"
    leaf1.vm.box_version = "3.7.3"    
    leaf1.vm.network("forwarded_port", guest: 22, host: 2024, id: 'ssh', auto_correct: false)

    leaf1.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF1", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    leaf1.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF1", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)

    # K8S on leaf1
    leaf1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_MASTER1", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    leaf1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_NODE1", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)
    leaf1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_NODE2", cumulus__intname: 'swp5', nic_type: "virtio", auto_config: false)
  end

  config.vm.define("leaf2") do |leaf2|
    leaf2.vm.box = "CumulusCommunity/cumulus-vx"
    leaf2.vm.box_version = "3.7.3"
    leaf2.vm.network("forwarded_port", guest: 22, host: 2025, id: 'ssh', auto_correct: false)

    leaf2.vm.network("private_network", virtualbox__intnet: "SPINE1_LEAF2", cumulus__intname: 'swp1', nic_type: "virtio", auto_config: false)
    leaf2.vm.network("private_network", virtualbox__intnet: "SPINE2_LEAF2", cumulus__intname: 'swp2', nic_type: "virtio", auto_config: false)

    # K8S on leaf2
    leaf2.vm.network("private_network", virtualbox__intnet: "K8S_LEAF2_NODE1", cumulus__intname: 'swp3', nic_type: "virtio", auto_config: false)
    # On-prem cluster on leaf2
    leaf2.vm.network("private_network", virtualbox__intnet: "ONPREM_LEAF2_NODE1", cumulus__intname: 'swp4', nic_type: "virtio", auto_config: false)
  end

  # Kubernetes 
  config.vm.define("k8s-master-l1-1") do |k8smaster1|
    k8smaster1.vm.box = "centos/7"
    k8smaster1.vm.hostname = "k8s-master-l1-1"
    k8smaster1.vm.network("forwarded_port", guest: 22, host: 3001, id: 'ssh', auto_correct: false)
    k8smaster1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_MASTER1", nic_type: "virtio")
    k8smaster1.vm.provider "virtualbox" do |vb|
      vb.cpus = 4
      vb.memory = 4096
    end 

    k8smaster1.vm.provision "file", source: "k8s-provisioning/ip.k8s-master-l1-1.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/containerd.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_containerd.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/kubeadm-init.sh", destination: "$HOME/k8s-provisioning/05_kubeadm-init.sh"
    k8smaster1.vm.provision "file", source: "k8s-provisioning/labels.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/06_labels.k8s-nodes.sh"
    k8smaster1.vm.provision "file", source: "calico-with-peering/", destination: "$HOME/calico-with-peering"
    k8smaster1.vm.provision "file", source: "vdb-op-setup/", destination: "$HOME/vdb-op-setup"
    k8smaster1.vm.provision "file", source: "vdb-op-setup/vertica.conf", destination: "/tmp/vertica.conf"
    k8smaster1.vm.provision "shell",
      inline: "mv /tmp/vertica.conf /etc/sysctl.d/vertica.conf"
    k8smaster1.vm.provision "file", source: "vdb-op-setup/30-nofile.conf", destination: "/tmp/30-nofile.conf"
    k8smaster1.vm.provision "shell", 
      inline: "mv /tmp/30-nofile.conf /etc/security/limits.d/30-nofile.conf"
  end

  config.vm.define("k8s-node-l1-1") do |k8snode1|
    k8snode1.vm.box = "centos/7"
    k8snode1.vm.hostname = "k8s-node-l1-1"
    k8snode1.vm.network("forwarded_port", guest: 22, host: 3002, id: 'ssh', auto_correct: false)
    k8snode1.vm.network("private_network", virtualbox__intnet: "K8S_LEAF1_NODE1", nic_type: "virtio")
    k8snode1.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.memory = 2048
    end 
    k8snode1.vm.provision "file", source: "k8s-provisioning/ip.k8s-node-l1-1.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8snode1.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8snode1.vm.provision "file", source: "k8s-provisioning/containerd.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_containerd.k8s-nodes.sh"
    k8snode1.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
  end

  config.vm.define("k8s-node-l2-1") do |k8snode3|
    k8snode3.vm.box = "centos/7"
    k8snode3.vm.hostname = "k8s-node-l2-1"
    k8snode3.vm.network("forwarded_port", guest: 22, host: 3004, id: 'ssh', auto_correct: false)
    k8snode3.vm.network("private_network", virtualbox__intnet: "K8S_LEAF2_NODE1", nic_type: "virtio")
    k8snode3.vm.provider "virtualbox" do |vb|
      vb.cpus = 1
      vb.memory = 2048
    end 
    k8snode3.vm.provision "file", source: "k8s-provisioning/ip.k8s-node-l2-1.sh", destination: "$HOME/k8s-provisioning/01_ip.k8s-node.sh"
    k8snode3.vm.provision "file", source: "k8s-provisioning/etc.hosts.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/02_etc.hosts.k8s-nodes.sh"
    k8snode3.vm.provision "file", source: "k8s-provisioning/containerd.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/03_containerd.k8s-nodes.sh"
    k8snode3.vm.provision "file", source: "k8s-provisioning/init.k8s-nodes.sh", destination: "$HOME/k8s-provisioning/04_init.k8s-nodes.sh"
  end

  config.vm.define("vertica-onprem-l2-1") do |vertica|
    vertica.vm.box = "centos/7"
    vertica.vm.hostname = "vertica-onprem-l2-1"
    vertica.vm.network("forwarded_port", guest: 22, host: 3006, id: 'ssh', auto_correct: false)
    vertica.vm.network("private_network", virtualbox__intnet: "ONPREM_LEAF2_NODE1", nic_type: "virtio")
    vertica.vm.provider "virtualbox" do |vb|
      vb.cpus = 4
      vb.memory = 4096
    end 

    bootstrap = <<SCRIPT
      groupadd verticadba
      useradd -m dbadmin --groups verticadba
      usermod -g verticadba dbadmin
      echo "dbadmin ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/dbadmin
      cp -r /home/vagrant/.ssh/ /home/dbadmin
      chown -R dbadmin:verticadba /home/dbadmin/.ssh
SCRIPT

    vertica.vm.provision "shell", inline: "#{bootstrap}", privileged: true
    VAGRANT_COMMAND = ARGV[0]
    if VAGRANT_COMMAND == "ssh"
      vertica.ssh.username = 'dbadmin'
    end

    vertica.vm.provision "file", source: "create-on-prem-vertica/", destination: "/home/vagrant/create-on-prem-vertica"
    vertica.vm.provision "shell", 
      inline: "mv /home/vagrant/create-on-prem-vertica /home/dbadmin/create-on-prem-vertica"
    vertica.vm.provision "file", source: "scale-vertica/", destination: "/home/vagrant/scale-vertica"
    vertica.vm.provision "shell", 
      inline: "mv /home/vagrant/scale-vertica/ /home/dbadmin/scale-vertica"
  end
end
