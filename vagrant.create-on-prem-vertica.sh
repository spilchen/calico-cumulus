#!/bin/sh

set -o errexit
set -o xtrace

PRIV_KEY=$(cat $(vagrant ssh-config vertica-onprem-l2-1 | grep IdentityFile | sed 's/  IdentityFile //'))

H=vertica-onprem-l2-1
RPM=vertica-x86_64.RHEL6.latest.rpm
vagrant ssh $H -c "sudo ./create-on-prem-vertica/01*"
vagrant ssh $H -c "./create-on-prem-vertica/02* '$PRIV_KEY'"
vagrant ssh $H -c "./create-on-prem-vertica/03*"
vagrant ssh $H -c "./create-on-prem-vertica/04*"
vagrant ssh $H -c "./create-on-prem-vertica/05*"
vagrant ssh $H -c "./create-on-prem-vertica/06*"
