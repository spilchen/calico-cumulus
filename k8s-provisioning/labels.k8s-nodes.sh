#!/bin/sh

kubectl label nodes k8s-master-l1-1 "asnum=65101"
kubectl label nodes k8s-node-l1-1   "asnum=65101"
kubectl label nodes k8s-node-l2-1   "asnum=65102"
