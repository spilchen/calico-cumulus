#!/bin/sh

set -o xtrace
set -o errexit

swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# Set SELinux in permissive mode (effectively disabling it)
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

systemctl enable --now kubelet

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system


systemctl daemon-reload
systemctl restart kubelet

systemctl stop firewalld
systemctl disable firewalld

# Create the 'k' alias
cat <<EOF >  /usr/local/bin/k
kubectl \$@
EOF
chmod +x /usr/local/bin/k

# Install kubens
curl -L -O https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens
chmod +x kubens
mv kubens /usr/local/bin

# Install bash completions for kubectl/kubens
yum install -y bash-completion
kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null
curl -L -O https://raw.githubusercontent.com/ahmetb/kubectx/7560b8f04f42469a156759504f601dd585d82f3c/completion/kubens.bash
mv kubens.bash /etc/bash_completion.d
echo 'complete -o default -F __start_kubectl k' >> /home/vagrant/.bashrc
