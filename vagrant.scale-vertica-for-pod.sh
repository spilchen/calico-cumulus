#!/bin/sh

set -o errexit

if [ -z "$1" ]
then
  echo "*** No IP specified.  Available IPs are:"
  vagrant ssh k8s-master-l1-1 -c "kubectl get pods -n vertica --selector demo=bgp -o jsonpath='{range .items[*]}{.status.podIP}{\"\n\"}{end}'"
  exit 1
fi

set -o xtrace
vagrant ssh vertica-onprem-l2-1 -c "./scale-vertica/01-scale.sh $1"
