#!/bin/sh

set -o xtrace
set -o errexit

vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-master-l1-1 'asnum=65101'"
vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-node-l1-1   'asnum=65101'"
vagrant ssh k8s-master-l1-1 -c "kubectl label nodes k8s-node-l2-1   'asnum=65102'"

vagrant ssh k8s-master-l1-1 -c "kubectl apply -f calico-with-peering/calicoctl.yaml"
vagrant ssh k8s-master-l1-1 -c "kubectl create -f calico-with-peering/crds.yaml"
vagrant ssh k8s-master-l1-1 -c "kubectl apply -f calico-with-peering/calico.yaml"

vagrant ssh k8s-master-l1-1 -c "while ! kubectl get pods -n kube-system calicoctl; do sleep 5; done"
vagrant ssh k8s-master-l1-1 -c "kubectl wait -n kube-system --for=condition=Ready=True pod calicoctl --timeout 5m"

vagrant ssh k8s-master-l1-1 -c "kubectl exec -i -n kube-system calicoctl -- calicoctl apply -f - < calico-with-peering/calico.nodes.yaml"
vagrant ssh k8s-master-l1-1 -c "kubectl exec -i -n kube-system calicoctl -- calicoctl apply -f - < calico-with-peering/calico.bgpconfig.yaml"

# Disable NAT in the IPPool.  Vertica depends on this to work as any
# NAT will cause issues for spread -- it discards messages from an IP
# it doesn't recognize.
vagrant ssh k8s-master-l1-1 -c "while ! kubectl get ippool.crd.projectcalico.org/default-ipv4-ippool; do sleep 5; done"
vagrant ssh k8s-master-l1-1 -c "kubectl patch ippools default-ipv4-ippool --type=merge --patch '{\"spec\": {\"natOutgoing\": false}}'"

#alias calicoctl="kubectl exec -i -n kube-system calicoctl /calicoctl -- "
