#!/bin/sh

nmcli con modify "System eth0" ipv4.address "10.0.102.102/24"
nmcli con modify "System eth0" ipv4.routes "10.0.0.0/8 10.0.102.1"
nmcli con modify "System eth0" ipv4.method manual
nmcli con down "System eth0"
nmcli con up "System eth0"

echo "10.0.101.10  k8s-master-l1-1" >> /etc/hosts
echo "10.0.101.101 k8s-node-l1-1" >> /etc/hosts
echo "10.0.102.101 k8s-node-l2-1" >> /etc/hosts
echo "10.0.102.102 vertica-onprem-l2-1" >> /etc/hosts
