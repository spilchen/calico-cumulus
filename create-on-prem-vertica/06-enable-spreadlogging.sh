#!/bin/sh

set -o errexit
set -o xtrace
set -o pipefail


vsql -c "SELECT set_spread_options('/opt/vertica/log/spread.log', 'MEMBERSHIP PRINT CONFIGURATION GROUPS SESSION PROTOCOL EXIT', 'ExitOnIdle = yes');"
vsql -c "SELECT reload_spread(true);"
