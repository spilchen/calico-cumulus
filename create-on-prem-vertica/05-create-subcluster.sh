#!/bin/sh

set -o errexit
set -o xtrace
set -o pipefail

/opt/vertica/bin/admintools \
    -t db_add_subcluster \
    --database vertdb \
    --subcluster k8s \
    --is-secondary
