#!/bin/sh

PRIV_KEY=$1
echo "$PRIV_KEY" > .ssh/id_rsa
chmod 0600 .ssh/id_rsa
ssh-keygen -y -f .ssh/id_rsa > .ssh/id_rsa.pub
cat .ssh/id_rsa.pub >> .ssh/authorized_keys
sudo cp -r /home/dbadmin/.ssh /root

