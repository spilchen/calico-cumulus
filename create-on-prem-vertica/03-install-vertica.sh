#!/bin/sh

set -o errexit
set -o xtrace
set -o pipefail

RPM=vertica-x86_64.RHEL6.latest.rpm
if [ -f "/vagrant/$RPM" ]
then
    RPM="/vagrant/$RPM"
fi

if ! [ -f $RPM ]
then 
    curl https://vertica-community-edition-for-testing.s3.amazonaws.com/XCz9cp7m/vertica-12.0.3-0.x86_64.RHEL6.rpm -o $RPM
fi
sudo yum install -y $RPM || :

DATA_DIR=/data
DEPOT_DIR=/depot
sudo mkdir -p $DATA_DIR
sudo mkdir -p $DEPOT_DIR
sudo chmod a+rwx $DATA_DIR $DEPOT_DIR

sudo /opt/vertica/sbin/install_vertica \
    --accept-eula \
    --hosts 10.0.102.102 \
    --dba-user-password-disabled \
    --failure-threshold NONE \
    --point-to-point \
    --data-dir $DATA_DIR
