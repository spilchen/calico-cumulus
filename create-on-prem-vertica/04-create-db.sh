#!/bin/sh

set -o errexit
set -o xtrace
set -o pipefail

cat << EOF > /home/dbadmin/auth_parms.conf
awsauth = minio:minio123
awsendpoint = k8s-master-l1-1:30001
awsenablehttps = 0
EOF
sudo chown dbadmin:verticadba auth_parms.conf
sudo chmod a+rwx auth_parms.conf

/opt/vertica/bin/admintools \
    -t create_db \
    --hosts 10.0.102.102 \
    --communal-storage-location=s3://nimbusdb/db-$$ \
    -x /home/dbadmin/auth_parms.conf \
    --shard-count=12 \
    --depot-path=/depot \
    --database vertdb
