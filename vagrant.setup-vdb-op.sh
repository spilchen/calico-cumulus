#!/bin/sh

set -o errexit

OPERATOR_IMG_REPO=$1
OPERATOR_IMG_NAME=$2

# The ssh pods will have a different key from the on-prem cluster. However, the
# authorized_keys will include the pods key as well as the on-prem key.
ONPREM_PRIV_KEY=$(cat $(vagrant ssh-config vertica-onprem-l2-1 | grep IdentityFile | sed 's/  IdentityFile //'))
K8S_PRIV_KEY=$(cat $(vagrant ssh-config k8s-master-l1-1 | grep IdentityFile | sed 's/  IdentityFile //'))

vagrant ssh k8s-master-l1-1 -c 'sh vdb-op-setup/01*'
vagrant ssh k8s-master-l1-1 -c "sh vdb-op-setup/03* $OPERATOR_IMG_REPO $OPERATOR_IMG_NAME"
vagrant ssh k8s-master-l1-1 -c 'sh vdb-op-setup/04*'
vagrant ssh k8s-master-l1-1 -c 'sh vdb-op-setup/05*'
vagrant ssh k8s-master-l1-1 -c "sh vdb-op-setup/06* '$ONPREM_PRIV_KEY' '$K8S_PRIV_KEY'"
vagrant ssh k8s-master-l1-1 -c 'sh vdb-op-setup/07*'

