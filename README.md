# Vertica Hybrid Kubernetes Cluster

This repository contains a demo that sets up a Vertica on-prem cluster with a secondary subcluster running in Kubernetes.  It uses BGP to connect to the two networks.  

In order to test out BGP, we deploy Cumulus VX with IP Fabric -- IP Fabric provides IP reachablility between k8s nodes.  Kubernetes is then deployed on this network, and we use Calico to enable and configure BGP in Kubernetes.  This repo was modified from a [blog post](https://blog.container-solutions.com/prototyping-on-premises-network-infrastructure-for-workloads-running-on-kubernetes-clusters-part-1) that demonstrated BGP and Calico, you can refer to that blog for more background on this network setup.

## High level diagram of the setup
![Network Topology](images/network-topology.png "Network Topology")

## Deploying Vertica Cluster

### Prerequisites
1. Install the latest Vagrant engine
2. Install Virtualbox 
3. 16GB of RAM and 4CPUs are recommended to run this demo
4. Install python 2.7+ with virtualenv
5. Install sshpass and ansible

### Bringing IP Fabric up
1. Clone this repository and run following commands from inside of it
2. Bring all Cumulus VX VMs with ```vagrant up leaf1 spine1 leaf2 spine2``` command
3. IP Fabric: ```./fabric.deploy.sh```

### Deploy Kubernetes and Calico
1. Bring kubernetes VMs up with ```vagrant up k8s-master-l1-1 k8s-node-l1-1 k8s-node-l2-1``` command
2. Verify with ```vagrant status```
3. Prepare K8s nodes and take note of token and cert digest: ```./vagrant.prepare_nodes.sh```
4. Join K8s workers to the master: ```./vagrant.join_nodes.sh <token> <cert digest>```
5. Setup Calico with BGP Peering: ```./vagrant.calico-with-peering.sh```

### Setup Vertica 
1. Create the vm that will run the Vertica on-prem cluster: ```vagrant up vertica-onprem-l2-1```
2. Deploy the VerticaDB operator, minio for communal storage and pods that will eventually have the secondary subcluster: ```./vagrant.setup-vdb-op.sh```
3. Install Vertica and create the DB in the on-prem cluster: ```./vagrant.create-on-prem-vertica.sh```

### Scale Out Vertica

At this point Vertica should be setup on the vm vertica-onprem-l2-1 as a 1 node cluster.  To continue the demo, we will scale out to new nodes that are running as pods in Kubernetes.  The pods have already been deployed.  We just need to install and run Vertica on them.  You need the IP of the pod for the next step.  As a convience, it you call `vagrant.scale-vertica-for-pod.sh` without any arguments it will list the pod IPs.

```
$ ./vagrant.scale-vertica-for-pod.sh
*** No IP specified.  Available IPs are:
10.67.209.19
10.68.86.134
Connection to 127.0.0.1 closed.
$ ./vagrant.scale-vertica-for-pod.sh 10.67.209.19
$ ./vagrant.scale-vertica-for-pod.sh 10.68.86.134
$ vagrant ssh vertica-onprem-l2-1 -c "/opt/vertica/bin/admintools -t list_allnodes"
 Node              | Host         | State | Version          | DB
-------------------+--------------+-------+------------------+--------
 v_vertdb_node0001 | 10.0.102.102 | UP    | vertica-11.0.0.0 | vertdb
 v_vertdb_node0002 | 10.67.209.19 | UP    | vertica-11.0.0.0 | vertdb
 v_vertdb_node0003 | 10.68.86.134 | UP    | vertica-11.0.0.0 | vertdb
```

## Credit

This tutorial is modified from the one documented in this [blog post](https://blog.container-solutions.com/prototyping-on-premises-network-infrastructure-for-workloads-running-on-kubernetes-clusters-part-1) and this [git repo](https://gitlab.com/ContainerSolutions/calico-cumulus).  It has been modified specifically for the Vertica use case I was trying to test out.

