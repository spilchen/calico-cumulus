#!/bin/sh

set -o errexit
set -o xtrace
set -o pipefail

sudo /opt/vertica/sbin/update_vertica \
    --accept-eula \
    --add-hosts $1 \
    --dba-user-password-disabled \
    --failure-threshold NONE \
    --no-system-configuration \
    --point-to-point \
    --data-dir /data \
    --dba-user dbadmin \
    --no-package-checks \
    --no-ssh-key-install

/opt/vertica/bin/admintools \
    -t db_add_node \
    --hosts $1 \
    --database vertdb \
    --subcluster k8s \
    --noprompt
