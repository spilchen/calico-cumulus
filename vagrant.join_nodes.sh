#!/bin/sh

HOSTS='k8s-node-l1-1 k8s-node-l2-1'

for i in $HOSTS; do
    vagrant ssh $i -c "sudo kubeadm join 10.0.101.10:6443 --token $1 --discovery-token-ca-cert-hash $2"
done

