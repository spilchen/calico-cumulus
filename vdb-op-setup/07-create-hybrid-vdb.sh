#!/bin/sh

set -o xtrace
set -o errexit

kubectl delete -n vertica -f vdb-op-setup/vdb.yaml || :
kubectl apply -n vertica -f vdb-op-setup/vdb.yaml

PODS=2
POD_PREFIX=k8s-sc1-

# Wait for the pod to be created.  We can't use kubectl wait because the Ready
# condition will not be set.  We are just going to wait the phase to be
# running.
for i in $(seq 0 $(($PODS - 1)))
do
    while ! kubectl get -n vertica pod ${POD_PREFIX}$i
    do
      sleep 5
    done
    while ! kubectl get -n vertica pod ${POD_PREFIX}$i -o yaml | grep -q 'phase: Running'
    do
      sleep 5
    done
done

set +o xtrace

GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'  # No color

printf "${GREEN}Pod IPs${NC}\n"
for i in $(seq 0 $(($PODS - 1)))
do
    IP=$(kubectl get -n vertica pod ${POD_PREFIX}$i -o jsonpath="{.status.podIP}")
    printf "  ${YELLOW}$IP${NC}\n"
done
