#!/bin/sh

set -o xtrace
set -o errexit

OPERATOR_IMG_REPO=$1
OPERATOR_IMG_NAME=$2
if [[ -n "$OPERATOR_IMG_NAME" ]]
then
    HELM_OVERRIDE="--set image.name=$OPERATOR_IMG_NAME --set image.repo=$OPERATOR_IMG_REPO"
fi

helm repo add vertica-charts https://vertica.github.io/charts
helm repo update
helm uninstall vdb-op --namespace vertica || true
helm install --wait vdb-op --namespace vertica --create-namespace vertica-charts/verticadb-operator $HELM_OVERRIDE
