#!/bin/sh

set -o errexit
set -o xtrace
set -o pipefail

ONPREM_PRIV_KEY=$1
K8S_PRIV_KEY=$2

mkdir -p k8s-ssh
echo "$K8S_PRIV_KEY" > k8s-ssh/id_rsa
chmod 0600 k8s-ssh/id_rsa
ssh-keygen -y -f k8s-ssh/id_rsa > k8s-ssh/id_rsa.pub
cat k8s-ssh/id_rsa.pub >> k8s-ssh/authorized_keys
echo "$ONPREM_PRIV_KEY" > k8s-ssh/onprem_id_rsa
chmod 0600 k8s-ssh/onprem_id_rsa
ssh-keygen -y -f k8s-ssh/onprem_id_rsa >> k8s-ssh/authorized_keys
kubectl delete -n vertica secret ssh-key || :
kubectl create -n vertica secret generic ssh-key --from-file=/home/vagrant/k8s-ssh
