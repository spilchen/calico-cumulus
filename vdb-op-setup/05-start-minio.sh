#!/bin/sh

set -o xtrace
set -o errexit

MINIO_RELEASE=4.3.5
sudo yum -y install wget
if [[ ! -f kubectl-minio_${MINIO_RELEASE}_linux_amd64 ]]
then
    wget https://github.com/minio/operator/releases/download/v${MINIO_RELEASE}/kubectl-minio_${MINIO_RELEASE}_linux_amd64
fi
sudo cp kubectl-minio_${MINIO_RELEASE}_linux_amd64 /usr/local/bin/kubectl-minio
sudo chmod +x /usr/local/bin/kubectl-minio
kubectl minio init --console-image minio/console:v0.9.8 --image minio/operator:v4.2.4

# The above command will create the CRD.  But there is a timing hole where the
# CRD is not yet registered with k8s, causing the tenant creation below to
# fail.  Add a wait until we know the CRD exists.
echo "Waiting for CRD to be created..."
while [[ $(kubectl api-resources --api-group=minio.min.io -o name | wc -l) = "0" ]]
do
    sleep 0.1
done

MANIFEST=create-tenant.yaml
cat << EOF > $MANIFEST
apiVersion: v1
kind: Secret
metadata:
  name: s3-auth
type: Opaque
data:
  # Access Key for MinIO Tenant, base64 encoded (echo -n 'minio' | base64)
  accesskey: bWluaW8=
  # Secret Key for MinIO Tenant, base64 encoded (echo -n 'minio123' | base64)
  secretkey: bWluaW8xMjM=
---
# MinIO Tenant Definition
apiVersion: minio.min.io/v2
kind: Tenant
metadata:
  name: minio
spec:
  image: minio/minio:RELEASE.2021-08-31T05-46-54Z
  imagePullPolicy: IfNotPresent
  credsSecret:
    name: s3-auth
  pools:
    - servers: 1
      volumesPerServer: 4
      volumeClaimTemplate:
        metadata:
          name: data
        spec:
          accessModes:
            - ReadWriteOnce
          resources:
            requests:
              storage: 250Mi
  mountPath: /export
  requestAutoCert: false
EOF
kubectl apply -n vertica -f $MANIFEST

echo "Waiting for Minio pod to be running..."
while ! kubectl get pods -n vertica minio-ss-0-0; do sleep 5; done
kubectl wait --for=condition=Ready=True -n vertica pod minio-ss-0-0 --timeout 5m

MANIFEST=create-bucket.yaml
cat << EOF > $MANIFEST
# Job to create a bucket in minio
apiVersion: batch/v1
kind: Job
metadata:
  name: create-s3-bucket
spec:
  ttlSecondsAfterFinished: 360
  template:
    spec:
      containers:
        - name: aws
          image: amazon/aws-cli:2.2.24
          command:
            [
              "bash",
              "-c",
              "aws s3 rb --endpoint http://minio s3://nimbusdb --force || :; aws s3 mb --endpoint http://minio s3://nimbusdb/db",
            ]
          env:
            - name: AWS_ACCESS_KEY_ID
              value: minio
            - name: AWS_SECRET_ACCESS_KEY
              value: minio123
            - name: AWS_EC2_METADATA_DISABLED
              value: 'true'
      restartPolicy: Never
EOF
kubectl apply -n vertica -f $MANIFEST

# Set nodePort in minio service so that we can access it outside of k8s
NODEPORT=30001
kubectl patch svc minio -n vertica --type=merge --patch '{"spec": {"type": "NodePort", "ports": [{"name": "http-minio", "port": 80, "targetPort": 9000, "nodePort": 30001}]}}'
echo "Minio svc has node port set to $NODEPORT"

# Create a svc so that we can access the endpoint from within the pod.  This
# just maps to an external name of the hard coded master node.
MANIFEST=ext-svc.yaml
cat << EOF > $MANIFEST
apiVersion: v1
kind: Service
metadata:
  name: k8s-master-l1-1
spec:
  clusterIP: None
  ports:
  - port: 30001
    protocol: TCP
    targetPort: 30001
  type: ClusterIP
---
apiVersion: v1
kind: Endpoints
metadata:
  name: k8s-master-l1-1
subsets:
  - addresses:
    - ip: 10.0.101.10
    ports:
    - port: 9376
EOF
kubectl apply -n vertica -f $MANIFEST
